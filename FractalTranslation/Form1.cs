﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FractalCTranslation
{
    public partial class Form1 : Form
    {

        class HSB
        {//djm added, it makes it simpler to have this code in here than in the C#
            public float rChan, gChan, bChan;
            public HSB()
            {
                rChan = gChan = bChan = 0;
            }
            public void fromHSB(float h, float s, float b)
            {
                float red = b;
                float green = b;
                float blue = b;
                if (s != 0)
                {
                    float max = b;
                    float dif = b * s / 255f;
                    float min = b - dif;

                    float h2 = h * 360f / 255f;

                    if (h2 < 60f)
                    {
                        red = max;
                        green = h2 * dif / 60f + min;
                        blue = min;
                    }
                    else if (h2 < 120f)
                    {
                        red = -(h2 - 120f) * dif / 60f + min;
                        green = max;
                        blue = min;
                    }
                    else if (h2 < 180f)
                    {
                        red = min;
                        green = max;
                        blue = (h2 - 120f) * dif / 60f + min;
                    }
                    else if (h2 < 240f)
                    {
                        red = min;
                        green = -(h2 - 240f) * dif / 60f + min;
                        blue = max;
                    }
                    else if (h2 < 300f)
                    {
                        red = (h2 - 240f) * dif / 60f + min;
                        green = min;
                        blue = max;
                    }
                    else if (h2 <= 360f)
                    {
                        red = max;
                        green = min;
                        blue = -(h2 - 360f) * dif / 60 + min;
                    }
                    else
                    {
                        red = 0;
                        green = 0;
                        blue = 0;
                    }
                }

                rChan = (float)Math.Round(Math.Min(Math.Max(red, 0f), 255));
                gChan = (float)Math.Round(Math.Min(Math.Max(green, 0), 255));
                bChan = (float)Math.Round(Math.Min(Math.Max(blue, 0), 255));

            }
        }



        private const int MAX = 256;      // max iterations
        private const double SX = -2.025; // start value real
        private const double SY = -1.125; // start value imaginary
        private const double EX = 0.6;    // end value real
        private const double EY = 1.125;  // end value imaginary
        private static int x1, y1, xs, ys, xe, ye;
        private static double xstart, ystart, xende, yende, xzoom, yzoom;
        private static bool action, rectangle, finished;
        private static float xy;
        // error // private Bitmap picture;
        // error // private Graphics g1;
        // error //  private Cursor c1, c2;
        private HSB HSBcol = new HSB();


        public void init() // all instances will be prepared
        {
            //HSBcol = new HSB();
            //xx/setSize(640, 480);
            finished = false;
            //xxx/addMouseListener(this);
            //xxx/addMouseMotionListener(this);
            //xxx/c1 = new Cursor(Cursor.WAIT_CURSOR);
            //xxx/c2 = new Cursor(Cursor.CROSSHAIR_CURSOR);
            //xxx/x1 = getSize().width;
            //xxx/y1 = getSize().height;
            xy = (float)x1 / (float)y1;
            //xxx/picture = createImage(x1, y1);
            //xxx/g1 = picture.getGraphics();
            finished = true;
        }

        public void destroy() // delete all instances 
        {
            if (finished)
            {
                //To Be Done//removeMouseListener(this);
                //To Be Done//removeMouseMotionListener(this);
                // error // picture = null;
                // error // g1 = null;
                // error // c1 = null;
                // error // c2 = null;
                //To Be Done// System.gc(); // garbage collection
            }
        }

        public void start()
        {
            action = false;
            rectangle = false;
            //To Be Done// initvalues();
            xzoom = (xende - xstart) / (double)x1;
            yzoom = (yende - ystart) / (double)y1;
            //To Be Done// mandelbrot();
        }

        public void stop()
        {
        }

       // error // public void paint(Graphics g)
        {
            //To Be Done// update(g);
        }

       // error // public void update(Graphics g)
        {
         // error //   g.drawImage(picture, 0, 0, this);
         // error //   if (rectangle)
            {
              // error //  g.setColor(Color.white);
             // error //   if (xs < xe)
                {
                  // error //  if (ys < ye) g.drawRect(xs, ys, (xe - xs), (ye - ys));
                  // error //  else g.drawRect(xs, ye, (xe - xs), (ys - ye));
                }
                else
                {
                  // error //  if (ys < ye) g.drawRect(xe, ys, (xs - xe), (ye - ys));
                   // error // else g.drawRect(xe, ye, (xs - xe), (ys - ye));
                }
            }
        }


        private void mandelbrot() // calculate all points
        {
            int x, y;
            float h, b, alt = 0.0f;

            action = false;
            // error //  setCursor(c1);
            // error // showStatus("Mandelbrot-Set will be produced - please wait...");
            for (x = 0; x < x1; x += 2)
                for (y = 0; y < y1; y++)
                {
                    h = pointcolour(xstart + xzoom * (double)x, ystart + yzoom * (double)y); // color value
                    if (h != alt)
                    {
                        b = 1.0f - h * h; // brightnes
                                          ///djm added
                                          ///HSBcol.fromHSB(h,0.8f,b); //convert hsb to rgb then make a Java Color
                                          ///Color col = new Color(0,HSBcol.rChan,HSBcol.gChan,HSBcol.bChan);
                                          ///g1.setColor(col);
                        //djm end
                        //djm added to convert to RGB from HSB

                        // error  g1.setColor(Color.getHSBColor(h, 0.8f, b));
                        //djm test
                        // error  Color col = Color.getHSBColor(h, 0.8f, b);
                        // error //  int red = col.getRed();
                        // error //  int green = col.getGreen();
                        // error //  int blue = col.getBlue();
                        //djm 
                        alt = h;
                    }
                    // error //  g1.drawLine(x, y, x + 1, y);
                }
            // error // showStatus("Mandelbrot-Set ready - please select zoom area with pressed mouse.");
            // error // setCursor(c2);
            action = true;
        }

        private float pointcolour(double xwert, double ywert) // color value from 0.0 to 1.0 by iterations
        {
            double r = 0.0, i = 0.0, m = 0.0;
            int j = 0;

            while ((j < MAX) && (m < 4.0))
            {
                j++;
                m = r * r - i * i;
                i = 2.0 * r * i + ywert;
                r = m + xwert;
            }
            return (float)j / (float)MAX;
        }

        private void initvalues() // reset start values
        {
            xstart = SX;
            ystart = SY;
            xende = EX;
            yende = EY;
            if ((float)((xende - xstart) / (yende - ystart)) != xy)
                xstart = xende - (yende - ystart) * (double)xy;
        }

        // error //  public void mousePressed(MouseEvent e)
        // error //   {
        // error //  e.consume();
        // error //  if (action)
        // error // {
        // error //   xs = e.getX();
        // error //   ys = e.getY();
    }
}

// error // public void mouseReleased(MouseEvent e)
// error // {
// error // int z, w;

// error //  e.consume();
// error //  if (action)
// error //  {
// error //  xe = e.getX();
// error //  ye = e.getY();
// error //  if (xs > xe)
// error // {
// error //  z = xs;
// error // xs = xe;
// error // xe = z;
// error //  }
// error //   if (ys > ye)
// error //  {
// error //   z = ys;
// error //  ys = ye;
// error //  ye = z;
// error //  }
// error //   w = (xe - xs);
// error //  z = (ye - ys);
// error //  if ((w < 2) && (z < 2)) initvalues();
// error // else
// error // {
// error //   if (((float)w > (float)z * xy)) ye = (int)((float)ys + (float)w / xy);
// error // else xe = (int)((float)xs + (float)z * xy);
// error //  xende = xstart + xzoom * (double)xe;
// error //  yende = ystart + yzoom * (double)ye;
// error // xstart += xzoom * (double)xs;
// error // ystart += yzoom * (double)ys;
// error //  }
// error // xzoom = (xende - xstart) / (double)x1;
// error //  yzoom = (yende - ystart) / (double)y1;
// error // mandelbrot();
// error // rectangle = false;
// error // repaint();
// error // }
// error // }

// error // public void mouseEntered(MouseEvent e)
// error // {
// error //  }

// error //  public void mouseExited(MouseEvent e)
// error //  {
// error // }

// error //  public void mouseClicked(MouseEvent e)
// error //  {
// error //   }

// error //  public void mouseDragged(MouseEvent e)
// error //  {
// error // e.consume();
// error // if (action)
// error // {
// error // xe = e.getX();
// error //  ye = e.getY();
// error // rectangle = true;
// error // repaint();
// error // }
// error //  }

// error // public void mouseMoved(MouseEvent e)
// error // {
// error //  }

// error // public String getAppletInfo()
// error // {
// error // return "fractal.class - Mandelbrot Set a Java Applet by Eckhard Roessel 2000-2001";
// error // }
// error // }


// error // public Form1()
// error // {
// error //  InitializeComponent();


// error //  }
// error // }
// error // }